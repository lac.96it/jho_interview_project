# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `MAC`
```sh
$ yarn install
$ yarn start
``` 

### `Windows`
```sh
$ npm install
$ npm start
``` 

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Login account
 Username: **admin**

 Password: **admin123**
