const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              '@primary-color': '#0046AA',
              '@divider-color': '#e5e5e5',
              '@checkbox-size': '18px',

              '@layout-pd-x': '20px',
              '@header-height': '64px',
              '@trading-bar-height': '58px',
              '@footer-height': '100px',
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};