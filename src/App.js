import React from 'react';
import { ConfigProvider } from 'antd';
import vi_VN from 'antd/es/locale-provider/vi_VN';
import Routes from './routes';

import './App.less';

function App() {
  return (
    <ConfigProvider locale={vi_VN}>
      <Routes />
    </ConfigProvider>
  );
}

export default App;
