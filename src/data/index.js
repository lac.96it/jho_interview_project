import data from './data.json';

export const getDataFromJSONByKey = (key) => {
    return data[key];
}

export const getDataNews = (pageSize) => {
    const listNewsData = getDataFromJSONByKey('list-news')
    let result = [];
    for(let i = 0; i < pageSize; i++) {
        let ranInt = Math.floor(Math.random() * listNewsData.length);

        result.push(listNewsData[ranInt]);
    }

    return result;
}