import { MenuOutlined, HeartOutlined, StarOutlined } from '@ant-design/icons';
import { Agricultural, AgriculturalMaterials, Energy, Metal, ViewByDate } from '../assets/icon';

const newsCategoryData = [
    { name: 'Tất cả', category: 'all', icon: MenuOutlined },
    { name: 'Yêu thích', category: 'favourite', icon: HeartOutlined },
    {
        name: 'Nông sản',
        category: 'agricultural',
        icon: Agricultural,
        children: [
            { name: 'Ngô', category: 'agricultural-1' },
            { name: 'Đậu tương', category: 'agricultural-2' },
            { name: 'Khô đậu tương', category: 'agricultural-3' },
            { name: 'Dầu đậu tương', category: 'agricultural-4' },
            { name: 'Lúa mỳ', category: 'agricultural-5' },
        ]
    },
    {
        name: 'Nguyên liệu nông nghiệp',
        category: 'agricultural-materials',
        icon: AgriculturalMaterials,
        children: [
            { name: 'Cà phê', category: 'agricultural-materials-1' },
            { name: 'Ca cao', category: 'agricultural-materials-2' },
            { name: 'Bông', category: 'agricultural-materials-3' },
            { name: 'Đường', category: 'agricultural-materials-4' },
            { name: 'Cao su', category: 'agricultural-materials-5' },
        ]
    },
    {
        name: 'Năng lượng',
        category: 'energy',
        icon: Energy,
        children: [
            { name: 'Dầu thô', category: 'energy-1' },
            { name: 'Khí tự nhiên', category: 'energy-2' },
            { name: 'Xăng', category: 'energy-3' },
        ]
    },
    {
        name: 'Kim loại',
        category: 'metal',
        icon: Metal,
        children: [
            { name: 'Bạc', category: 'metal-1' },
            { name: 'Bạch kim', category: 'metal-2' },
            { name: 'Đồng', category: 'metal-3' },
            { name: 'Quặng sắt', category: 'metal-4' },
        ]
    },
    { name: 'Mức độ quan trọng', category: 'critical-level', icon: StarOutlined },
    { name: 'Xem theo ngày', category: 'view-by-date', icon: ViewByDate },
];

export default newsCategoryData;