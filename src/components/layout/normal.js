import React from 'react';

import { Layout, Row, Col } from 'antd';

import Header from './header';
import Footer from './footer';
import TradingBar from './trading-bar';
import Advertisement from './advertisement';

import './normal-style.less';

const { Content } = Layout;

const NormalLayout = ({ children }) => {
    return (
        <Layout className="normal-layout">
            <Header />
            <TradingBar />
            <Content>
                <Row gutter={20}>
                    <Col flex={1}>{children}</Col>
                    <Advertisement />
                </Row>
            </Content>
            <Footer />
        </Layout>
    )
}

export default NormalLayout;