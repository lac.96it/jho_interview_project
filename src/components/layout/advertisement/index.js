import React from 'react';

import { Col } from 'antd';

import Adver1Img from '../../../assets/img/Adver-1.png';
import Adver2Img from '../../../assets/img/Adver-2.png';

import './style.less';

const Advertisement = () => {
    return (
        <Col className="advertisement-section">
            <img src={Adver1Img} alt="Advertisement 1" />
            <img src={Adver2Img} alt="Advertisement 1" />
        </Col>
    )
}

export default Advertisement;