import React from 'react';

import { Layout } from 'antd';

import UserInfo from './user-info';
import HeaderLogo from './logo';
import ActionBar from './action-bar';

import './style.less';

const { Header: AntHeader } = Layout;

const Header = () => {
    return (
        <AntHeader className="normal-header">
            <UserInfo />
            <HeaderLogo />
            <ActionBar />
        </AntHeader>
    )
}

export default Header;