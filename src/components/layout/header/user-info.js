import React from 'react';

import { Avatar, Space, Dropdown, Menu, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { ChevDown } from '../../../assets/icon';
import { useAuth } from '../../../routes/context';
import { getDataFromJSONByKey } from '../../../data';

const UserInfo = () => {

    const { logout } = useAuth();
    const userInfo = getDataFromJSONByKey('auth');

    const menu = (
        <Menu>
            <Menu.Item onClick={logout} danger>Logout</Menu.Item>
        </Menu>
    );

    return (
        <Space size={10} className="header-user-info">
            <Avatar size={32} icon={<UserOutlined />} />
            <div className="header-user-info__dropdown">
                <Dropdown
                    overlay={menu}
                    trigger={['click']}
                >
                    <Button type="text">
                        <span>{userInfo.name}</span>
                        <ChevDown />
                    </Button>
                </Dropdown>
            </div>
        </Space>
    )
}

export default UserInfo;