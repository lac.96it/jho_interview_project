import React from 'react';

import { Space, Input, Badge } from 'antd';
import { QuestionCircleOutlined, MailOutlined, BellOutlined } from '@ant-design/icons';

import { getDataFromJSONByKey } from '../../../data';

const ActionBar = () => {
    const actionBarData = getDataFromJSONByKey('notification');

    return (
        <Space size={30} className="header-action-bar" align="center">
            <Input.Search placeholder="Tìm kiếm..." enterButton />
            <QuestionCircleOutlined />
            <Badge count={actionBarData.mailCount}>
                <MailOutlined />
            </Badge>
            <Badge count={actionBarData.notificationCount}>
                <BellOutlined />
            </Badge>
        </Space>
    )
}

export default ActionBar;