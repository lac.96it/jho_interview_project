import React from 'react';

import headerLogo from '../../../assets/img/header-logo.svg';

const HeaderLogo  = () => {
    return <img className="header-logo" src={headerLogo} alt="MXV-Logo"/>
}

export default HeaderLogo;