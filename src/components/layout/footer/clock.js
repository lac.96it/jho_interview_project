import React, { useState, useEffect, memo } from 'react';

import { Button } from 'antd';
import { Calendar } from '../../../assets/icon';

const Clock = () => {

    const [curTime, setCurTime] = useState('');

    useEffect(() => {
        const intervalID = setInterval(
            () => setCurTime(`${new Date().toLocaleTimeString([], { hour12: false })}  -  ${new Date().toLocaleDateString()}`),
            1000
        );

        return () => clearInterval(intervalID);
    }, []);

    return <Button className="clock-btn" type={"primary"} block icon={<Calendar />}>{curTime}</Button>
}

export default memo(Clock);