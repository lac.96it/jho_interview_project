import React, { useCallback } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';

import { Layout, Button, Row, Col } from 'antd';
import routesData from './routes';
import ClockBtn from './clock';

import './style.less';

const { Footer: AntFooter } = Layout;

const Footer = () => {

    const history = useHistory();
    const match = useRouteMatch();

    const handleClick = (href) => {
        if (href) history.push(`${match.path}${href}`);
    }

    const getTyprOfBtn = useCallback((href) => {
        if (history.location.pathname === `${match.path}${href}`) return 'primary';

        if (history.location.pathname === `${match.path}` && href === '/news') return 'primary';

        return 'default';
    }, [history, match]);

    return (
        <AntFooter className="custom-footer">
            <Row gutter={27}>
                {routesData.map((item, idx) => (
                    <Col key={`menu-item-${idx}`} span={4}>
                        <Button
                            icon={item.icon}
                            onClick={() => handleClick(item.href)}
                            block
                            type={getTyprOfBtn(item.href)}
                        >
                            {item.title}
                        </Button>
                    </Col>
                ))}

                <Col span={4}>
                    <ClockBtn />
                </Col>
            </Row>
        </AntFooter>
    )
}

export default Footer;