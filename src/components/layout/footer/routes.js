import { PriceReport, Chart, News, Media, Tool } from '../../../assets/icon';

const routeData = [
    {
        title: 'Bảng giá',
        icon: <PriceReport />,
    },

    {
        title: 'Biểu đồ',
        icon: <Chart />,
    },

    {
        href: '/news',
        title: 'Tin tức',
        icon: <News />,
    },

    {
        href: '/media',
        title: 'Media',
        icon: <Media />,
    },

    {
        title: 'Công cụ',
        icon: <Tool />,
    }
]

export default routeData;