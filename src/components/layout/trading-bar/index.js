import React from 'react';

import { Layout, Carousel } from 'antd';
import TradingItem from './trading-item';
import {getDataFromJSONByKey} from '../../../data';

import './style.less';

const { Header } = Layout;

const TradingBar = () => {

    const tradingBarData = getDataFromJSONByKey('trading-bar');

    return (
        <Header className="trading-bar">
            <Carousel autoplay dots={false} slidesToShow={6} slidesToScroll={1} pauseOnHover infinite speed={3000} autoplaySpeed={1000}>
            {
                tradingBarData.map((item, idx) => (
                    <TradingItem tradingItem={item} key={`trading-item-${idx}`} />
                ))
            }
            </Carousel>
        </Header>
    )
}

export default TradingBar;