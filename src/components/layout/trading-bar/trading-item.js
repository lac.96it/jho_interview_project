import React from 'react';

import { ArrowDown, ArrowUp } from '../../../assets/icon';

const TradingItem = ({ tradingItem }) => {
    return (
        <div className="trading-item">
            <div className="flex-box">
                <div className="trading-item__name">{tradingItem.name}</div>
                <div className="trading-item__value">{tradingItem.value}<span className="trading-item__unit">{tradingItem.unit}</span></div>
            </div>
            <div className="value-box">
                <div
                    className={`trading-item__value-changed ${Number(tradingItem.valueChanged) > 0 ? 'positive' : 'negative'}`}
                >
                    {Number(tradingItem.valueChanged) > 0 ? <ArrowUp /> : <ArrowDown />}
                    {tradingItem.valueChanged}
                </div>
                <div
                    className={`trading-item__percent-value-changed ${Number(tradingItem.valueChanged) > 0 ? 'positive' : 'negative'}`}
                >
                    {tradingItem.percentValueChanged}%
                </div>
            </div>
        </div>
    )
}

export default TradingItem;