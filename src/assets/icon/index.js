import Icon from '@ant-design/icons';
import React from 'react';
import svg from './svg';

export const ChevDown = (props) => <Icon component={svg.ChevDown} {...props} />;
export const PriceReport = (props) => <Icon component={svg.PriceReport} {...props} />;
export const Chart = (props) => <Icon component={svg.Chart} {...props} />;
export const News = (props) => <Icon component={svg.News} {...props} />;
export const Media = (props) => <Icon component={svg.Media} {...props} />;
export const Tool = (props) => <Icon component={svg.Tool} {...props} />;
export const Calendar = (props) => <Icon component={svg.Calendar} {...props} />;
export const ArrowDown = (props) => <Icon component={svg.ArrowDown} {...props} />;
export const ArrowUp = (props) => <Icon component={svg.ArrowUp} {...props} />;
export const Agricultural = (props) => <Icon component={svg.Agricultural} {...props} />;
export const AgriculturalMaterials = (props) => <Icon component={svg.AgriculturalMaterials} {...props} />;
export const Energy = (props) => <Icon component={svg.Energy} {...props} />;
export const Metal = (props) => <Icon component={svg.Metal} {...props} />;
export const ViewByDate = (props) => <Icon component={svg.ViewByDate} {...props} />;
