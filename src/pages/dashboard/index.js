import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { NormalLayout } from '../../components';
import NewsPage from '../news';
import MediaPage from '../media';

const Dashboard = () => {

    const match = useRouteMatch();

    return (
        <NormalLayout>
            <Switch>
                <Redirect exact from={match.path} to={`${match.path}/news`} />

                <Route component={NewsPage} path={`${match.path}/news`} />

                <Route component={MediaPage} path={`${match.path}/media`} />

                <Redirect to={`/404`} />
            </Switch>
        </NormalLayout>
    )
}

export default Dashboard;