import React from 'react';

import { useListNews } from './context';

import Pagination from './pagination';

const Footer = () => {

    const { pagination } = useListNews();

    return (
        <div className="list-news-footer">
            <div className="footer-pagination-info">
                Hiển thị <span className="value-section">{(pagination.page - 1) * pagination.pageSize + 1} - {pagination.pageSize * pagination.page > pagination.total ? pagination.total : pagination.pageSize * pagination.page}</span> trong <span className="value-section">{pagination.total}</span> tin
            </div>
            <Pagination />
        </div>
    )
}

export default Footer;