import React from 'react';

import { Card, Divider } from 'antd';
import { ListNewsProvider } from './context';
import ListNewsTable from './list';
import Footer from './footer';

import './style.less';

const ListNews = () => {
    return (
        <Card title="TIN TỨC CẬP NHẬT" className="list-news">
            <ListNewsProvider>
                <ListNewsTable />
                <Divider />
                <Footer />
            </ListNewsProvider>
        </Card>
    )
}

export default ListNews;