import React, { useMemo } from 'react';

import { useListNews } from './context';
import { Button, InputNumber, Space } from 'antd';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';

const Pagination = () => {

    const { pagination, setPage } = useListNews();

    const numberOfPage = useMemo(() => {
        return Math.ceil(pagination.total / pagination.pageSize);
    }, [pagination.total, pagination.pageSize]);

    const gotoHead = () => setPage(1);
    const gotoEnd = () => setPage(numberOfPage);
    const handlePrev = () => setPage(pagination.page - 1);
    const handleNext = () => setPage(pagination.page + 1);

    const handleSetPageFromInput = (e) => {
        let value = e.target.value;

        if (value < 0) value = 0;
        if (value > numberOfPage) value = numberOfPage;

        setPage(value);
    }

    return (
        <div className="footer-pagination">
            <Space size={5}>
                <Button onClick={gotoHead} disabled={pagination.page <= 1}>Đầu</Button>
                <Button onClick={handlePrev} disabled={pagination.page <= 1} className="btn-icon"><LeftOutlined /></Button>
            </Space>
            <span className="pdx-12">Trang</span>
            <InputNumber
                value={pagination.page}
                onBlur={handleSetPageFromInput}
                min={1}
                max={numberOfPage}
            />
            <span className="pdx-12">/{numberOfPage}</span>
            <Space size={5}>
                <Button disabled={pagination.page >= numberOfPage} onClick={handleNext} className="btn-icon"><RightOutlined /></Button>
                <Button disabled={pagination.page >= numberOfPage} onClick={gotoEnd}>Cuối</Button>
            </Space>
        </div>
    )
}

export default Pagination;