import React from 'react';

import { List, Space, Tooltip, Row, Col, Tag } from 'antd';
import {StarFilled} from '@ant-design/icons';
import { useListNews } from './context';

const ListNewsTable = () => {

    const { listNews, pagination } = useListNews();

    return (
        <List
            dataSource={listNews}
            renderItem={(item, idx) => (
                <List.Item>
                    <div className="list-news__wrapper">
                        <Row className="list-news__content">
                            <Col span={16} className={`list-news__title ${pagination.page === 1 && idx === 0 && 'newsest'} ${pagination.page === 1 && idx === 1 && 'news2nd'}`}>
                                {pagination.page === 1 && (idx === 0 || idx === 1)&& <StarFilled />}

                                <Tooltip title={item.title}>
                                    {item.title}
                                </Tooltip>
                            </Col>
                            <Col span={8}>
                                <Space size={5} className="list-news__tags">
                                    <Tag color="processing" className={item.tags[0] === 'E' && 'e-tag'}>{item.tags[0]}</Tag>
                                    {item.tags.length > 1 && (
                                        <Tooltip title={item.tags.slice(1).join(' - ').toUpperCase()}>
                                            <Tag color="processing">+{item.tags.length - 1}</Tag>
                                        </Tooltip>
                                    )}
                                </Space>
                            </Col>
                        </Row>
                        <div className="list-news__date-time">
                            <Space size={15}>
                                <span className="list-news__time">{item.time}</span>
                                <span className="list-news__date">{item.date}</span>
                            </Space>
                        </div>
                    </div>
                </List.Item>
            )}
        />
    )
}

export default ListNewsTable;