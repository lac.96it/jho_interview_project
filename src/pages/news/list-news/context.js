import React, { createContext, useState, useEffect, useContext, useMemo } from 'react';
import { getDataNews } from '../../../data';

const initialContext = {
    pagination: {
        page: 1,
        pageSize: 50,
        total: 999
    },
    listNews: [],
    selectedNewsIndex: undefined,
    setListNews: listNews => listNews,
    setSelectedNewsIndex: selectedNewsIndex => selectedNewsIndex,
    setPagination: pagination => pagination,
}

const ListNewsContext = createContext(initialContext);

export const ListNewsProvider = ({ children }) => {
    const [pagination, setPagination] = useState({
        page: 1,
        pageSize: 50,
        total: 999
    });
    const [listNews, setListNews] = useState([]);
    const [selectedNewsIndex, setSelectedNewsIndex] = useState();

    useEffect(() => {
        setListNews(getDataNews(pagination.pageSize));
    }, [pagination]);

    const value = useMemo(() => ({
        pagination,
        listNews,
        selectedNewsIndex,
        setPagination,
        setListNews,
        setSelectedNewsIndex
    }), [
        pagination,
        listNews,
        selectedNewsIndex,
        setPagination,
        setListNews,
        setSelectedNewsIndex
    ]);

    return <ListNewsContext.Provider value={value}>{children}</ListNewsContext.Provider>
}

export const useListNews = () => {
    const {
        pagination, listNews, selectedNewsIndex,
        setPagination
    } = useContext(ListNewsContext);

    const setPage = (page) => {
        setPagination({
            ...pagination,
            page
        });
    }

    return {
        pagination,
        listNews,
        selectedNewsIndex,
        setPage
    }
}