import React from 'react';

import { Card, Menu } from 'antd';

import newsCategoryData from '../../data/news-category';

const {SubMenu} = Menu;

const Category = () => {
    return (
        <Card title="DANH MỤC TIN TỨC" className="category-list">
            <Menu
                defaultOpenKeys={[]}
                mode="inline"
            >
                {newsCategoryData.map((category, idx) => {
                    if (category.children)
                        return (
                            <SubMenu key={category.category} icon={<category.icon />} title={category.name}>
                                {category.children.map(cateChild => (
                                    <Menu.Item key={cateChild.category}>{cateChild.name}</Menu.Item>
                                ))}
                            </SubMenu>
                        )

                    return (
                        <Menu.Item key={category.category} icon={<category.icon />}>
                            {category.name}
                        </Menu.Item>
                    )
                })}
            </Menu>
        </Card>
    )
}

export default Category;