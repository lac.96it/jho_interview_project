import React from 'react';

import { Row, Col } from 'antd';
import Category from './category';
import ListNews from './list-news';

import './style.less';

const News = () => {
    return (
        <Row>
            <Col span={6}><Category /></Col>
            <Col span={18}><ListNews /></Col>
        </Row>
    )
}

export default News;