import React from 'react';

import { Form, Input, Checkbox, Typography, Button, Space } from 'antd';

import { useAuth } from '../../routes/context';
import { getDataFromJSONByKey } from '../../data';

const LoginForm = () => {

    const [form] = Form.useForm();

    const { login } = useAuth();

    const loginData = getDataFromJSONByKey('auth');

    const handleLogin = (values) => {
        const { email, password } = values;

        if (email === loginData.email && password === loginData.password) login();
        else {
            if (email !== loginData.email)
                form.setFields([
                    {
                        name: 'email',
                        errors: ['Email không chính xác']
                    }
                ]);

            if (password !== loginData.password)
                form.setFields([
                    {
                        name: 'password',
                        errors: ['Mật khẩu không chính xác']
                    }
                ]);
        }
    }

    return (
        <div className="login-form">
            <div className="login-form__title">ĐĂNG NHẬP TÀI KHOẢN</div>

            <Form layout="vertical" onFinish={handleLogin} form={form}>
                <Form.Item label="Email" name="email" rules={[{ required: true, message: 'Xin hãy nhập email' }]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Mật khẩu" name="password" rules={[{ required: true, message: 'Xin hãy nhập mật khẩu!' }]}>
                    <Input.Password />
                </Form.Item>
                <Space>
                    <Form.Item name="remember_me" valuePropName="checked" noStyle>
                        <Checkbox>Ghi nhớ tài khoản</Checkbox>
                    </Form.Item>

                    <Typography.Link>Quên mật khẩu</Typography.Link>
                </Space>
                <Button htmlType="submit" type="primary" block className="btn-login">
                    Đăng nhập
                </Button>

                <Typography.Text type="secondary" className="help-lead-text">
                    Cần trợ giúp?
                </Typography.Text>
            </Form>
        </div>
    )
}

export default LoginForm;