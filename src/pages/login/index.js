import React from 'react';

import Introduce from './introduce';
import LoginForm from './login-form';
import { Divider, Typography } from 'antd';

import logo from '../../assets/img/logo.png';
import './style.less';

const LoginPage = () => {
    return (
        <div className="login-container">
            <div className="login-wrapper">
                <Introduce />

                <div className="login-section">
                    <div className="login-section-img">
                        <img src={logo} alt="MXV-Logo" />
                    </div>

                    <Divider />

                    <LoginForm />

                    <Divider />
                    <div className="register-account-lead">Bạn chưa có tài khoản trên MXV? <Typography.Link>Đăng ký</Typography.Link></div>
                </div>
             </div>
        </div>
    )
}

export default LoginPage;