import React from 'react';

import logoIcon from '../../assets/img/logo-icon.png';

const Introduce = () => {
    return (
        <div className="introduce">
            <div className="introduce-img">
                <img src={logoIcon} alt="MXV-Logo"/>
            </div>
            <div className="introduce-content">
                <div className="introduce-title">
                    "Bệ phóng mới" cho thị trường kỳ hạn Việt Nam
                </div>
                <div className="introduce-content-body">
                    <p>Với vai trò là tổ chức đầu tiên thực hiện việc giao dịch hàng hoá
                        một cách quy mô, hiện đại, MXV có sứ mệnh trở thành cổng kết nối trung gian 
                        uy tín và duy nhất của Việt Nam ra thi trường hàng hoá quốc tế.
                    </p>
                    <p>
                        MXV tin rằng việc tăng cường kiểm soát rủi ro và tăng cường tính quốc tế hoá thị trường
                        giao dịch hàng hoá Việt Nam phát huy những lợi thế cạnh tranh ngành nông sản, nguyên liệu,
                        thúc đẩy thị trường kỳ hạn tại Việt Nam bước vào một kỷ nguyên mới.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default Introduce;