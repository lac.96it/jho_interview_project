import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAuth } from './context';

const UnAuthRoute = ({ children, ...rest }) => {
    const { isAuth } = useAuth();

    return (
        <Route
            {...rest}
            render={({ location }) =>
                !isAuth ? (
                    children
                ) : (
                        <Redirect
                            to={{
                                pathname: '/dashboard',
                                state: { from: location },
                            }}
                        />
                    )
            }
        />
    );
};

const AuthRoute = ({ children, ...rest }) => {
    const { isAuth } = useAuth();

    return (
        <Route
            {...rest}
            render={({ location }) =>
                isAuth ? (
                    children
                ) : (
                        <Redirect
                            to={{
                                pathname: '/login',
                                state: { from: location },
                            }}
                        />
                    )
            }
        />
    );
};

export { UnAuthRoute, AuthRoute };
