import React, { Suspense } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { UnAuthRoute, AuthRoute } from './hoc-route';
import { Loading } from '../components';
import { AuthProvider } from './context';

const LoginPage = React.lazy(() => import('../pages/login'));
const NotFound = React.lazy(() => import('../pages/not-found'));
const Dashboard = React.lazy(() => import('../pages/dashboard'));

const Routes = () => {

    return (
        <AuthProvider>
            <Suspense fallback={<Loading full />}>
                <Router>
                    <Switch>
                        <Redirect exact from='/' to={`/dashboard`} />
                        <AuthRoute path='/dashboard'>
                            <Dashboard />
                        </AuthRoute>

                        <UnAuthRoute path='/login'>
                            <LoginPage />
                        </UnAuthRoute>

                        <Route path="/404" component={NotFound} />

                        <Redirect to={`/404`} />
                    </Switch>
                </Router>
            </Suspense>
        </AuthProvider>
    );
};

export default Routes;
