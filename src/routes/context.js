import React, { createContext, useState, useEffect, useMemo, useContext, useCallback } from 'react';

const initialContext = {
    isAuth: false,
    setIsAuth: isAuth => isAuth
}

const AuthContext = createContext(initialContext);

export const AuthProvider = ({ children }) => {
    const [isAuth, setIsAuth] = useState(false);

    useEffect(() => {
        let localIsAuth = localStorage.getItem('isAuth')

        if (localIsAuth) setIsAuth(localIsAuth === 'true');

        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        localStorage.setItem('isAuth', isAuth);
        // eslint-disable-next-line
    }, [isAuth]);

    const value = useMemo(() => ({
        isAuth,
        setIsAuth
    }), [isAuth, setIsAuth]);

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}

export const useAuth = () => {
    const { isAuth, setIsAuth } = useContext(AuthContext);

    const login = useCallback(() => {
        setIsAuth(true);
        // eslint-disable-next-line
    }, []);

    const logout = useCallback(() => {
        setIsAuth(false);
        // eslint-disable-next-line
    }, []);

    return {
        isAuth,
        login,
        logout
    }
}